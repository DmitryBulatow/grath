package com.company.graph2;

import java.util.HashMap;
import java.util.List;

public class Matrix {

	public static void createByMatrix(int[][] adjacencyMatrix, List<Point> points){
		createPoints(adjacencyMatrix, points);
		createConnectPointsCost(adjacencyMatrix, points);
	}

	public static void createPoints(int[][] adjacencyMatrix, List<Point> points) {
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			points.add(new Point(i));
		}
	}

	public static void createConnectPointsCost(int[][] adjacencyMatrix, List<Point> points) {
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			HashMap<Point, Integer> connectsPointsCost = new HashMap<>();
			for (int j = 0; j < adjacencyMatrix[i].length; j++) {
				if (adjacencyMatrix[i][j]>0){
					connectsPointsCost.put(points.get(j), adjacencyMatrix[i][j]);
				}
			}
			points.get(i).setConnectsPointsCost(connectsPointsCost);
		}
	}

	public static void reCreateCost(Point minPoint, int min) {
		HashMap<Point,Integer> hashMap = minPoint.getConnectsPointsCost();
		for (Point pointSet  : hashMap.keySet()) {
			hashMap.replace(pointSet, hashMap.get(pointSet)+min);

		}
		minPoint.setConnectsPointsCost(hashMap);
	}

	public static void reCreateMinLength(Point minPoint, HashMap<Integer, Integer> minLengthToPoint) {
		for (Point tempPoint: minPoint.getConnectsPointsCost().keySet()) {
			minLengthToPoint.replace(minLengthToPoint.get(tempPoint), minPoint.getConnectsPointsCost().get(minPoint));
		}
	}

}
