package com.company.graph2;

import java.util.HashMap;
import java.util.Objects;

public class Point implements Comparable<Point>{

	private boolean isFound = false;
	private int pointNumber;
	private HashMap<Point, Integer> connectsPointsCost;

	public Point(int pointNumber) {
		this.pointNumber = pointNumber;
	}

	public boolean isFound() {
		return isFound;
	}

	public void setFound(boolean wasTaken) {
		isFound = wasTaken;
	}

	public int getPointNumber() {
		return pointNumber;
	}


	public HashMap<Point, Integer> getConnectsPointsCost() {
		return connectsPointsCost;
	}

	public void setConnectsPointsCost(HashMap<Point, Integer> connectsPointsCost) {
		this.connectsPointsCost = connectsPointsCost;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Point point = (Point) o;
		return 	pointNumber == point.pointNumber;
	}

	@Override
	public int hashCode() {
		return Objects.hash(pointNumber);
	}


		@Override
	public int compareTo(Point o) {
		return String.valueOf(this.getPointNumber()).compareTo(String.valueOf(o.getPointNumber()));
	}

	@Override
	public String toString() {
		return "Point{" +
				"pointNumber=" + pointNumber +
				'}';
	}
}
