package com.company.graph2;

import java.util.Objects;

public class PairOfObjects<E>
 {
 	private E firstObject;
 	private E secondObject;

	 public PairOfObjects(E firstObject, E secondObject) {
		 this.firstObject = firstObject;
		 this.secondObject = secondObject;
	 }

	 public E getPair(E object){
		if (object==firstObject){
			return secondObject;
		}else if (object == secondObject){
			return firstObject;
		}else {
			return null;
		}
	 }

	 public E getFirstObject() {
		 return firstObject;
	 }

	 public E getSecondObject() {
		 return secondObject;
	 }

	 @Override
	 public boolean equals(Object o) {
		 if (this == o) return true;
		 if (o == null || getClass() != o.getClass()) return false;
		 PairOfObjects<?> that = (PairOfObjects<?>) o;
		 return (Objects.equals(firstObject, that.firstObject) &&
				 Objects.equals(secondObject, that.secondObject))||
				 (Objects.equals(firstObject, that.secondObject) &&
				 Objects.equals(secondObject, that.firstObject));
	 }

	 @Override
	 public int hashCode() {
		 return Objects.hash(Integer.parseInt(String.valueOf(firstObject))+Integer.parseInt(String.valueOf(secondObject)));
	 }
	 public boolean equalsBuOneElement(Object o){
		 if (this == o) return true;
		 if (o == null || getClass() != o.getClass()) return false;
		 PairOfObjects<?> that = (PairOfObjects<?>) o;
		 return  Objects.equals(firstObject, that.firstObject) ||
				 Objects.equals(secondObject, that.secondObject)||
				 Objects.equals(firstObject, that.secondObject) ||
				 Objects.equals(secondObject, that.firstObject);
	 } 
	 
	 public boolean equalsBuOnlyOneElement(Object o){
		 if (this == o) return true;
		 if (o == null || getClass() != o.getClass()) return false;
		 PairOfObjects<?> that = (PairOfObjects<?>) o;
		 return  (Objects.equals(firstObject, that.firstObject) ^
				 Objects.equals(secondObject, that.secondObject)) ^
				 (Objects.equals(firstObject, that.secondObject) ^  
				 Objects.equals(secondObject, that.firstObject));
	 }
 }
