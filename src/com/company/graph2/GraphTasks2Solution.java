package com.company.graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
	    List<Point> points = new ArrayList<>();
	    Matrix.createByMatrix(adjacencyMatrix,points);
	    points.get(startIndex).setFound(true);
	    return minLengthByDexter(new HashMap<Integer, Integer>(),points);
    }

	private HashMap<Integer, Integer> createHashMap(int startIndex, int size) {
		HashMap<Integer, Integer> minLengthToPoint = new HashMap<>();
		for (int i = 0; i < size; i++) {
			if (i==startIndex){
				minLengthToPoint.put(startIndex, 0);
			}else {
				minLengthToPoint.put(i,Integer.MAX_VALUE);
			}
		}
		return minLengthToPoint;
	}

	private HashMap<Integer, Integer> minLengthByDexter(HashMap<Integer, Integer> minLengthToPoint, List<Point> points) {

		int min = Integer.MAX_VALUE;
		Point minPoint = null;

		for (int i = 0; i < points.size(); i++) {

			if (points.get(i).isFound()){

				for (Point pointSet  : points.get(i).getConnectsPointsCost().keySet()) {
					if (!pointSet.isFound()){
						if (min >= points.get(i).getConnectsPointsCost().get(pointSet)){
							min = points.get(i).getConnectsPointsCost().get(pointSet);
							minPoint = points.get(pointSet.getPointNumber());
						}
					}
				}
			}
		}


		if (Objects.isNull(minPoint)){
			return minLengthToPoint;
		}


		minLengthToPoint.put(minPoint.getPointNumber(),min);
		Matrix.reCreateCost(minPoint, min);
		minPoint.setFound(true);

		return minLengthByDexter(minLengthToPoint, points);
	}

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
	    List<Point> points = new ArrayList<>();

	    HashSet<Point> pointHashSet = new HashSet<>();

	    Matrix.createByMatrix(adjacencyMatrix, points);
	    pointHashSet.add(points.get(0));

	    return primAlgorithm(pointHashSet,0);
    }

	private int primAlgorithm(HashSet<Point> pointHashSet, int count) {
		int min = Integer.MAX_VALUE;
		Point minPoint = null;

		for (Point point: pointHashSet) {

			for (Point pointKeys : point.getConnectsPointsCost().keySet()) {
				if (!pointHashSet.contains(pointKeys)){
					if (min >= pointKeys.getConnectsPointsCost().get(point)){
						min = pointKeys.getConnectsPointsCost().get(point);
						minPoint = pointKeys;
					}
				}
			}
		}

		if (minPoint==null){
			return count;
		}

		pointHashSet.add(minPoint);
		return primAlgorithm(pointHashSet,count+min);
	}
    @Override
    public Integer /*maybe Long*/kraskalAlgorithm(int[][] adjacencyMatrix) {
	    List<Point> points = new ArrayList<>();
	    Matrix.createByMatrix(adjacencyMatrix,points);
	    List<HashSet<Point>> treesList = new ArrayList<>();
	    return kruskalAlgorithm(treesList, points, 0);
    }

	private int /*long*/ kruskalAlgorithm(List<HashSet<Point>> treesList, List<Point> points, int /*long*/ count){
		int min = Integer.MAX_VALUE;
		PairOfObjects<Point> minPair = null;
		for (int i = 0; i < points.size(); i++) {
			Point thisPoint = points.get(i);
			for (Point connectPoint: thisPoint.getConnectsPointsCost().keySet()) {

				if (thisPoint.getConnectsPointsCost().get(connectPoint) < min) {
					min = thisPoint.getConnectsPointsCost().get(connectPoint);
					minPair = new PairOfObjects<>(thisPoint, points.get(connectPoint.getPointNumber()));


				}
			}
		}


		int firstTreeIndex = -1;
		int secondTreeIndex = -1;


		for (int j = 0; j < treesList.size()&& secondTreeIndex==-1; j++) {
			if (firstTreeIndex ==-1 && (treesList.get(j).contains(minPair.getFirstObject()))){
				firstTreeIndex = j;
				if (treesList.get(j).contains(minPair.getSecondObject())){
					secondTreeIndex = j;
				}
			}else if (firstTreeIndex ==-1 && (treesList.get(j).contains(minPair.getSecondObject()))){
				firstTreeIndex = j;
				if (treesList.get(j).contains(minPair.getFirstObject())){
					secondTreeIndex = j;
				}
			}else if (firstTreeIndex !=-1 && (treesList.get(j).contains(minPair.getFirstObject()) || treesList.get(j).contains(minPair.getSecondObject()))){
				secondTreeIndex = j;
			}
		}
		if (firstTreeIndex==-1){
			HashSet<Point> newTree = new HashSet<>();
			newTree.add(minPair.getFirstObject());
			newTree.add(minPair.getSecondObject());
			treesList.add(newTree);
		}
		else if (secondTreeIndex==-1){
			HashSet<Point> addingSecond = null;
			HashSet<Point> addingFirst = null;
			for (Point p: treesList.get(firstTreeIndex)) {
				if (p== minPair.getFirstObject()){
					addingSecond = treesList.get(firstTreeIndex);
				}else if (p== minPair.getSecondObject()){
					addingFirst = treesList.get(firstTreeIndex);
				}
			}
			if (Objects.nonNull(addingSecond)){
				addingSecond.add(minPair.getSecondObject());
			}else {
				addingFirst.add(minPair.getFirstObject());
			}
		}
		else if (firstTreeIndex!=secondTreeIndex){
			HashSet<Point> concatTree = new HashSet<>();
			concatTree.addAll(treesList.get(firstTreeIndex));
			concatTree.addAll(treesList.get(secondTreeIndex));
			treesList.remove(secondTreeIndex);
			treesList.remove(firstTreeIndex);
			treesList.add(concatTree);
		}else {
			min=0;
		}
		points.get(minPair.getFirstObject().getPointNumber()).getConnectsPointsCost().remove(minPair.getSecondObject());
		points.get(minPair.getSecondObject().getPointNumber()).getConnectsPointsCost().remove(minPair.getFirstObject());

		if (treesList.get(0).size()== points.size()){
			return count+min;
		}

		return kruskalAlgorithm(treesList,points,count+min);
	}

}
